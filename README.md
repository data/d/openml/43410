# OpenML dataset: Coronavirus-Disease-(COVID-19)

https://www.openml.org/d/43410

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Since awareness on COVID-19   began growing across the world, more health datasets have been published as open for (re-)users to utilise in creating platforms and interactive maps, for example, to support citizens in taking steps to stay healthy, like avoiding risk areas  .
This dataset is intended to mobilize researchers   to apply recent advances in natural language processing to generate new insights in support of the fight against this infectious disease.
Acknowledgements
Source of data: ourworldindata

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43410) of an [OpenML dataset](https://www.openml.org/d/43410). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43410/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43410/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43410/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

